package com.progrssoft;

import java.util.Arrays;

public class Matrix {
    private final int[][] matrix;
    private final int columns;
    private final int rows;

    public Matrix(int[][] matrix) {
        failIfMatrixIsInvalid(matrix);

        this.matrix = copyMatrix(matrix);
        this.rows = matrix.length;
        this.columns = matrix[0].length;
    }

    private int[][] copyMatrix(int[][] matrix) {
        int[][] tempArray = new int[matrix.length][];

        for (int row = 0; row < matrix.length; row++) {
            tempArray[row] = Arrays.copyOf(matrix[row], matrix[row].length);
        }
        return tempArray;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    public int getColumns() {
        return columns;
    }

    public int getRows() {
        return rows;
    }

    public static boolean isMatricesSameSize(int[][] matrixA, int[][] matrixB) {
        if (matrixA.length != matrixB.length) return false;

        for (int row = 0; row < matrixA.length; row++) {
            if (matrixA[row].length != matrixB[row].length)
                return false;
        }
        return true;
    }

    public boolean isValidMatrix(int[][] matrix) {
        if (isHaveNullRow(matrix)) return false;

        int cellCount = 0;
        int rows = matrix.length;
        for (int[] row : matrix) {
            cellCount += row.length;
        }

        return cellCount % rows == 0;
    }

    private void throwIllegalArgumentException(String message) {
        throw new IllegalArgumentException(message);
    }

    private boolean isHaveNullRow(int[][] matrix) {
        for (int[] row : matrix) {
            if (row == null) return true;
        }

        return false;
    }

    private boolean isNullArray(int[][] matrix) {
        return matrix != null;
    }

    public int getCell(int row, int col) {
        return matrix[row][col];
    }

    private void failIfMatrixIsInvalid(int[][] matrixA) {
        if (!isNullArray(matrixA))
            throwIllegalArgumentException("Invalid Matrix array is null");

        if (isHaveNullRow(matrixA))
            throwIllegalArgumentException("Matrix should not contain NULL Row");

        if (!isValidMatrix(matrixA))
            throwIllegalArgumentException("Matrix is not valid");
    }

}
