package com.progrssoft;

//TODO refactor to a more appropriate name
public class MatrixOperations {

    //TODO matrixA.length and matrixA[0].length should be extracted and reused
    //TODO refactor I and J variables to more descriptive names
    //TODO Methods names should be actions
    //TODO use Matrix object instead int array
    //TODO: remove duplicated validate from this class

    public static Matrix sum(Matrix matrixA, Matrix matrixB) {
        failIfNotValidMatricesForSummation(matrixA, matrixB);

        int rows = matrixA.getRows();
        int columns = matrixA.getColumns();
        int[][] answer = new int[rows][columns];

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                answer[row][col] = matrixA.getMatrix()[row][col] + matrixB.getMatrix()[row][col];
            }
        }

        return new Matrix(answer);
    }

    public static Matrix multiply(Matrix matrixA, int multiplicationBy) {
        int rows = matrixA.getRows();
        int columns = matrixA.getColumns();
        int[][] answer = new int[rows][columns];

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < matrixA.getMatrix()[0].length; col++) {
                answer[row][col] = matrixA.getMatrix()[row][col] * multiplicationBy;
            }
        }

        return new Matrix(answer);
    }

    public static Matrix multiply(Matrix matrixA, Matrix matrixB) {
        failIfNotValidForMultiplication(matrixA, matrixB);

        int rows = matrixA.getRows();
        int columns = matrixA.getColumns();
        int[][] answer = new int[rows][columns];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                answer[i][j] = multiplyCell(matrixA.getMatrix(), matrixB.getMatrix(), i, j);
            }
        }

        return new Matrix(answer);
    }

    private static int multiplyCell(int[][] matrixA, int[][] matrixB, int row, int col) {
        int cell = 0;

        for (int k = 0; k < matrixB.length; k++) {
            cell += matrixA[row][k] * matrixB[k][col];
        }

        return cell;
    }

    public static Matrix transpose(Matrix matrixA) {
        int rows = matrixA.getRows();
        int columns = matrixA.getColumns();
        int[][] answer = new int[columns][rows];

        for (int row = 0; row < answer.length; row++) {
            for (int col = 0; col < answer[0].length; col++) {
                answer[row][col] = matrixA.getMatrix()[col][row];
            }
        }

        return new Matrix(answer);
    }

    public static Matrix subMatrix(Matrix matrixA) {
        return subMatrix(matrixA, 0, 0);
    }

    public static Matrix subMatrix(Matrix matrix, int startRow, int startCol) {
        int rows = matrix.getRows();
        int columns = matrix.getColumns();

        if (startCol > columns || startRow > rows || startRow < 0 || startCol < 0)
            throwIllegalArgumentException("The start cell for subMatrix is invalid");

        int[][] subArray = new int[rows - 1][columns - 1];

        for (int row = 0, answerRow = 0; row < rows; row++) {
            if (row == startRow) continue;
            for (int col = 0, answerCol = 0; col < columns; col++) {
                if (col == startCol) continue;
                subArray[answerRow][answerCol] = matrix.getMatrix()[row][col];
                answerCol++;
            }
            answerRow++;
        }

        return new Matrix(subArray);
    }

    public static Matrix diagonalMatrix(Matrix matrixA) {
        failIfMatrixIsNotSymmetric(matrixA);

        int rows = matrixA.getRows();
        int columns = matrixA.getColumns();
        int[][] answer = new int[rows][columns];

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                if (row != col)
                    answer[row][col] = 0;
                else
                    answer[row][col] = matrixA.getCell(row, col);
            }
        }
        return new Matrix(answer);
    }

    public static Matrix triangularMatrix(Matrix matrixA) {
        failIfMatrixIsNotSymmetric(matrixA);

        int rows = matrixA.getRows();
        int columns = matrixA.getColumns();
        int[][] answer = new int[rows][columns];

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                if (row != col && !(row > col))
                    answer[row][col] = 0;
                else
                    answer[row][col] = matrixA.getCell(row, col);
            }
        }

        return new Matrix(answer);
    }

    public static Matrix upperMatrix(Matrix matrixA) {
        failIfMatrixIsNotSymmetric(matrixA);

        int rows = matrixA.getRows();
        int columns = matrixA.getColumns();
        int[][] answer = new int[rows][columns];

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                if (row > col)
                    answer[row][col] = 0;
                else
                    answer[row][col] = matrixA.getCell(row, col);
            }
        }

        return new Matrix(answer);
    }

    private static int determinantMatrix(Matrix matrixA) {
        failIfMatrixIsNotSymmetric(matrixA);

        return matrixA.getCell(0, 0) * matrixA.getCell(1, 1) - matrixA.getCell(0, 1) * matrixA.getCell(1, 0);
    }

    public static int determinantMatrixNxN(Matrix matrixA) {
        failIfMatrixIsNotSymmetric(matrixA);

        int row = matrixA.getRows();
        if (row == 2) return determinantMatrix(matrixA);
        int determinant = 0;

        for (int i = 0; i < row; i++) {
            int cofactor = matrixA.getCell(0, i);
            if (i % 2 == 0)
                determinant += cofactor * determinantMatrixNxN(subMatrix(matrixA, 0, i));
            else
                determinant -= cofactor * determinantMatrixNxN(subMatrix(matrixA, 0, i));

        }

        return determinant;
    }

    private static boolean isMatricesSameSize(Matrix matrixA, Matrix matrixB) {
        if (matrixA.getRows() != matrixB.getRows()) return false;

        for (int row = 0; row < matrixA.getRows(); row++) {
            if (matrixA.getMatrix()[row].length != matrixB.getMatrix()[row].length)
                return false;
        }

        return true;
    }

    private static boolean isSymmetricMatrix(int[][] matrix) {
        int row = matrix.length;

        for (int[] rows : matrix) {
            if (rows.length != row)
                return false;
        }

        return true;
    }

    private static void throwIllegalArgumentException(String message) {
        throw new IllegalArgumentException(message);
    }

    private static boolean isNotCompatibleMatrix(int[][] matrixA, int[][] matrixB) {
        return (matrixA[0].length != matrixB.length);
    }

    private static void failIfNotValidMatricesForSummation(Matrix matrixA, Matrix matrixB) {
        if (!isMatricesSameSize(matrixA, matrixB))
            throwIllegalArgumentException("The Matrix Should Be Symmetric");
    }

    private static void failIfNotValidForMultiplication(Matrix matrixA, Matrix matrixB) {
        if (isNotCompatibleMatrix(matrixA.getMatrix(), matrixB.getMatrix()))
            throwIllegalArgumentException("Columns in first matrix not equal to rows in second matrix");
    }

    private static void failIfMatrixIsNotSymmetric(Matrix matrix) {
        if (!isSymmetricMatrix(matrix.getMatrix()))
            throwIllegalArgumentException("The Matrix Should Be Symmetric");
    }
}




