package com.progressoft;

import com.progrssoft.Matrix;
import com.progrssoft.MatrixOperations;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MatrixOperationsTest {

    @Test
    public void givenTwoValidMatrices_whenSum_thenReturnedExpectedResult() {
        Matrix matrixASum = new Matrix(new int[][]{
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}
        });
        Matrix matrixBSum = new Matrix(new int[][]{
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}
        });
        Matrix expectedResult = new Matrix(new int[][]{
                {2, 2, 2},
                {2, 2, 2},
                {2, 2, 2}
        });

        Matrix result = MatrixOperations.sum(matrixASum, matrixBSum);

        Assertions.assertArrayEquals(expectedResult.getMatrix(), result.getMatrix(), "returned result not as expected");
    }

    @Test
    public void givenValidMatricesWithDifferentSize_whenSum_thenExceptionIsThrown() {
        Matrix matrixASum = new Matrix(new int[][]{
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}
        });
        Matrix matrixBSum = new Matrix(new int[][]{
                {1, 1, 1},
                {1, 1, 1}
        });

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixOperations.sum(matrixASum, matrixBSum));
        Assertions.assertEquals("The Matrix Should Be Symmetric", exception.getMessage());
    }

    @Test
    public void givenValidMatrix_whenMultiply_thenReturnedExpectedResult() {
        Matrix matrixA = new Matrix(new int[][]{
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}
        });
        int multiplicationBy = 2;
        Matrix expectedResult = new Matrix(new int[][]{
                {2, 2, 2},
                {2, 2, 2},
                {2, 2, 2}
        });

        Matrix result = MatrixOperations.multiply(matrixA, multiplicationBy);

        Assertions.assertArrayEquals(expectedResult.getMatrix(), result.getMatrix(), "returned result not as expected");
    }

    @Test
    public void givenTwoValidMatrices_whenMultiply_thenReturnedExpectedResult() {
        Matrix matrixA = new Matrix(new int[][]{
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}
        });
        Matrix matrixB = new Matrix(new int[][]{
                {2, 2, 2},
                {2, 2, 2},
                {2, 2, 2}
        });

        Matrix expectedResult = new Matrix(new int[][]{
                {6, 6, 6},
                {6, 6, 6},
                {6, 6, 6}

        });

        Matrix result = MatrixOperations.multiply(matrixA, matrixB);

        Assertions.assertArrayEquals(expectedResult.getMatrix(), result.getMatrix(), "returned result not as expected");
    }

    @Test
    public void givenTwoValidMatricesWithDifferentSize_whenMultiply_thenReturnedExpectedResult() {
        Matrix matrixB = new Matrix(new int[][]{
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}
        });
        Matrix matrixA = new Matrix(new int[][]{
                {2, 2, 2},
                {2, 2, 2},
        });

        Matrix expectedResult = new Matrix(new int[][]{
                {6, 6, 6},
                {6, 6, 6}
        });

        Matrix result = MatrixOperations.multiply(matrixA, matrixB);

        Assertions.assertArrayEquals(expectedResult.getMatrix(), result.getMatrix(), "returned result not as expected");
    }


    @Test
    public void givenTowValidMatricesButNotRowAndColumnNotEqual_whenMultiply_thenExceptionIsThrown() {
        Matrix matrixB = new Matrix(new int[][]{
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}
        });
        Matrix matrixA = new Matrix(new int[][]{
                {2, 2},
                {2, 2},
                {2, 2}
        });

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, ()
                -> MatrixOperations.multiply(matrixA, matrixB));
        Assertions.assertEquals("Columns in first matrix not equal to rows in second matrix", exception.getMessage());
    }

    @Test
    public void givenValidMatrix_whenTranspose_thenReturnedExpectedResult() {
        Matrix matrix = new Matrix(new int[][]{
                {1, 1, 1},
                {1, 1, 1}
        });

        Matrix exceptedResult = new Matrix(new int[][]{
                {1, 1},
                {1, 1},
                {1, 1},
        });

        Assertions.assertArrayEquals(exceptedResult.getMatrix(), MatrixOperations.transpose(matrix).getMatrix());
    }

    @Test
    public void givenValidMatrix_whenSubMatrix_thenReturnedExpectedResult() {
        Matrix matrix = new Matrix(new int[][]{
                {1, 1, 1},
                {1, 1, 1}
        });

        Matrix exceptedResult = new Matrix(new int[][]{
                {1, 1},

        });

        Assertions.assertArrayEquals(exceptedResult.getMatrix(), MatrixOperations.subMatrix(matrix).getMatrix());
    }

    @Test
    public void givenValidMatrixWithValidStartRowAndStartColumn_whenSubMatrix_thenReturnedExpectedResult() {
        Matrix matrix = new Matrix(new int[][]{
                {1, 1, 1},
                {1, 1, 1}
        });

        Matrix exceptedResult = new Matrix(new int[][]{
                {1, 1},

        });
        int col = 1;
        int row = 1;

        Assertions.assertArrayEquals(exceptedResult.getMatrix(), MatrixOperations.subMatrix(matrix, row, col).getMatrix());
    }


    @Test
    public void givenValidMatrixWithInvalidStartRowOrStartColI_whenSubMatrix_thenExceptionIsThrown() {
        Matrix matrix = new Matrix(new int[][]{
                {1, 1, 1},
                {1, 1, 1}
        });

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixOperations.subMatrix(matrix, -1, 0));
        Assertions.assertEquals("The start cell for subMatrix is invalid", exception.getMessage());
    }

    @Test
    public void givenValidMatrix_whenDiagonalMatrix_thenReturnedExpectedResult() {

        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3},

        });
        Matrix expectedResult = new Matrix(new int[][]{
                {1, 0, 0},
                {0, 2, 0},
                {0, 0, 3}
        });
        Assertions.assertArrayEquals(expectedResult.getMatrix(), MatrixOperations.diagonalMatrix(matrix).getMatrix());
    }

    @Test
    public void givenValidMatrixWithInvalidSize_whenDiagonalMatrix_thenExceptionIsThrown() {

        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3, 4},
                {1, 2, 3, 4},
                {1, 2, 3, 4},
        });
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, ()
                -> MatrixOperations.diagonalMatrix(matrix));
        Assertions.assertEquals("The Matrix Should Be Symmetric", exception.getMessage());
    }

    @Test
    public void givenValidMatrix_whenUpperMatrix_thenReturnedExpectedResult() {

        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3},

        });
        Matrix expectedResult = new Matrix(new int[][]{
                {1, 2, 3},
                {0, 2, 3},
                {0, 0, 3}
        });
        Assertions.assertArrayEquals(expectedResult.getMatrix(), MatrixOperations.upperMatrix(matrix).getMatrix());
    }

    @Test
    public void givenValidMatrixWithInvalidSize_whenUpperMatrix_thenExceptionIsThrown() {

        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3, 4},
                {1, 2, 3, 4},
                {1, 2, 3, 4},
        });
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, ()
                -> MatrixOperations.upperMatrix(matrix));
        Assertions.assertEquals("The Matrix Should Be Symmetric", exception.getMessage());
    }

    @Test
    public void givenValidMatrix_whenTriangularMatrix_thenReturnedExpectedResult() {

        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3},
                {1, 2, 3},
                {1, 2, 3},

        });
        Matrix expectedResult = new Matrix(new int[][]{
                {1, 0, 0},
                {1, 2, 0},
                {1, 2, 3}
        });
        Assertions.assertArrayEquals(expectedResult.getMatrix(), MatrixOperations.triangularMatrix(matrix).getMatrix());
    }


    @Test
    public void givenValidMatrixWithInvalidSize_whenTriangularMatrix_thenExceptionIsThrown() {

        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3, 4},
                {1, 2, 3, 4},
                {1, 2, 3, 4},
        });
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, ()
                -> MatrixOperations.triangularMatrix(matrix));
        Assertions.assertEquals("The Matrix Should Be Symmetric", exception.getMessage());
    }


    @Test
    public void givenValidMatrix_whenDeterminantMatrixNxN_thenReturnedExpectedResult() {

        Matrix matrix = new Matrix(new int[][]{
                {2, 1, 3},
                {2, 0, 5},
                {3, 8, 9},
        });
        int expectedResult = -35;
        Assertions.assertEquals(expectedResult, MatrixOperations.determinantMatrixNxN(matrix));
    }

    @Test
    public void givenValidMatrixWithInvalidSize_whenDeterminantMatrixNxN_thenExceptionIsThrown() {

        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3, 4},
                {1, 2, 3, 4},
                {1, 2, 3, 4},
        });
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, ()
                -> MatrixOperations.determinantMatrixNxN(matrix));
        Assertions.assertEquals("The Matrix Should Be Symmetric", exception.getMessage());
    }

}
