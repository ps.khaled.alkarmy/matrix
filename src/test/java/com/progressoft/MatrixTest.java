package com.progressoft;

import com.progrssoft.Matrix;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MatrixTest {

    @Test
    public void givenNullArray_whenInitializeMatrix_thenExceptionIsTowns() {

        int[][] array = null;
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new Matrix(array));
        Assertions.assertEquals("Invalid Matrix array is null", exception.getMessage());
    }

    @Test
    public void givenInvalidArrayWithNullRow_whenInitializeMatrix_thenExceptionIsTowns() {

        int[][] array = new int[][]{
                {1, 2, 3},
                {1, 2, 3},
                null
        };
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new Matrix(array));
        Assertions.assertEquals("Matrix should not contain NULL Row", exception.getMessage());
    }

    @Test
    public void givenInvalidArray_whenInitializeMatrix_thenExceptionIsTowns() {
        int[][] array = new int[][]{
                {1, 2, 3},
                {1, 2},
        };
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new Matrix(array));
        Assertions.assertEquals("Matrix is not valid", exception.getMessage());
    }

    @Test
    public void givenValidArray_whenInitializeMatrix_thenExpectedResult() {

        int[][] array = new int[][]{
                {1, 2, 3},
                {1, 2, 3},
        };
        Matrix matrix = new Matrix(array);
        int expectedRow = 2;
        int expectedColumns = 3;
        int expectedCell = 1;
        array[0][0] = 5;
        Assertions.assertEquals(expectedRow, matrix.getRows());
        Assertions.assertEquals(expectedColumns, matrix.getColumns());
        Assertions.assertEquals(expectedCell, matrix.getCell(0, 0));

    }
}
